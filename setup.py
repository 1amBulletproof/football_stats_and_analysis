from setuptools import setup

setup(
   name='football_stats_and_analysis',
   version='1.0',
   description='Analyze football stats',
   author='Brandon Tarney',
   packages=['football_stats_and_analysis'],  #same as name
   install_requires=[
       'beautifulsoup4',
       'certifi',
       'charset-normalizer',
       'idna',
       'numpy',
       'pandas',
       'python-dateutil',
       'pytz',
       'requests',
       'six',
       'soupsieve',
       'urllib3']
   )

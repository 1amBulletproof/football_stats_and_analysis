#/bin/python

#library to scrape web data 

from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import requests as requests


def scrape():
    url = "https://fbref.com/en/squads/cff3d9bb/2022-2023/matchlogs/all_comps/schedule/Chelsea-Scores-and-Fixtures-All-Competitions"
    response = requests.get(url)
    print(response.text)


if __name__ == "__main__":
    scrape()
